# URLShortener backend

**URLShortener** is a fast URL Shortener

See in action: https://url.itunix.eu

## Getting started

1. PHP 5.3.x is required
2. Install Klein.php using Composer
3. Setup [URL rewriting](https://gitlab.itunix.eu/URLShortener/backend/blob/master/public/.htaccess) so that all requests are handled by **index.php**
4. Rename app_example.ini to app.ini and set your admin password

## Composer Installation

1. Get [Composer](http://getcomposer.org/)
2. Require Klein with `php composer.phar install`

## Set permission db for web server

```
$ cd /path/to/backend
$ chgrp www-data data/
$ chgrp www-data data/shortener.db
$ chmod g+w data/
$ chmod g+w data/shortener.db
```