<?php

header("Access-Control-Allow-Origin: *");

require '../vendor/autoload.php';
$klein = new \Klein\Klein();

$klein->respond(function ($request, $response, $service, $app) use ($klein) {

    $app->register('db', function() {
        return new PDO('sqlite:'.dirname(__FILE__).'/../data/shortener.db');
    });

    // bulid the datebase if not exist
    if (filesize(dirname(__FILE__).'/../data/shortener.db') == 0) {
        $app->db->query("CREATE TABLE IF NOT EXISTS urls (
                    id INTEGER PRIMARY KEY,
                    long TEXT,
                    short TEXT,
                    username TEXT
                )");
        $app->db->query("CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY,
            username TEXT,
            apikey TEXT,
            allowed TEXT
        )");
        $app->db->query("INSERT INTO users (username, apikey, allowed) VALUES ('admin', '123456', 'allow')");
    }
});

$klein->respond('GET', '/', function ($request, $response, $service) {
    $service->render('../views/index.phtml');
});

$klein->respond('GET', '/[:url]', function ($request, $response, $service, $app) {
    return getUrl($request->url, $preview=false, $response, $service, $app);
});

$klein->respond('GET', '/[:url]/preview', function ($request, $response, $service, $app) {
    return getUrl($request->url, $preview=true, $response, $service, $app);
});

$klein->respond('POST', '/shorten', function ($request, $response, $service, $app) {
    $url = filter_var($request->long_url, FILTER_SANITIZE_URL);

    if (!isset($request->long_url) && $request->long_url == "" )  { return $response->code(422)->append('No valid field'); }
    if (!isset($request->apikey)   && $request->apikey == "" )    { return $response->code(422)->append('No valid field'); }
    if (!filter_var($url, FILTER_VALIDATE_URL))                   { return $response->code(422)->append("$request->long_url is not a valid URL"); }
    if (strlen($request->long_url) > 2000)                        { return $response->code(422)->append('Max 2000 characters'); }

    // check user account
    $stmt = $app->db->prepare("SELECT * FROM users WHERE apikey = :apikey");
    $stmt->execute(array(':apikey'=>$request->apikey));
    $user = $stmt->fetch();
    if ($user['allowed'] != 'allow') { return $response->code(401)->append('No valid api key'); }

    // check exist urls
    $stmt = $app->db->prepare("SELECT * FROM urls WHERE long = :long_url");
    $stmt->execute(array(':long_url'=>$request->long_url));
    $result_url = $stmt->fetch();

    if (isset($result_url) && $result_url > 0) {
        $short = $result_url['short'];
        return $response->code(200)->append('https://url.itunix.eu/' . $short);
    } else {
        $longUrl = md5($request->long_url . time());
        $short = substr($longUrl, 0, 5);

        $qry = $app->db->prepare('INSERT INTO urls (long, short, username) VALUES (?, ?, ?)');
        $qry->execute(array($request->long_url, $short, $user['username']));

        return $response->code(201)->append('https://url.itunix.eu/' . $short);
    }

});

$klein->respond('POST', '/custom', function ($request, $response, $service, $app) {
    $url = filter_var($request->long_url, FILTER_SANITIZE_URL);

    if (!isset($request->short) && $request->short == "" )        { return $response->code(422)->append('No valid field'); }
    if (!isset($request->long_url) && $request->long_url == "" )  { return $response->code(422)->append('No valid field'); }
    if (!isset($request->apikey)   && $request->apikey == "" )    { return $response->code(422)->append('No valid field'); }
    if (!filter_var($url, FILTER_VALIDATE_URL))                   { return $response->code(422)->append("$request->long_url is not a valid URL"); }
    if (strlen($request->long_url) > 2000)                        { return $response->code(422)->append('Max 2000 characters'); }
    if (strlen($request->short) > 25)                             { return $response->code(422)->append('Shortname max 25 characters'); }

    if (!ctype_alnum($request->short)) {
        return $response->code(424)->append('Only alphanumeric characters allowed');
    }

    // check user account
    $stmt = $app->db->prepare("SELECT * FROM users WHERE apikey = :apikey");
    $stmt->execute(array(':apikey'=>$request->apikey));
    $user = $stmt->fetch();
    if ($user['allowed'] != 'allow') { return $response->code(401)->append('No valid api key'); }
    
    // check exist short
    $stmt = $app->db->prepare("SELECT * FROM urls WHERE short = :short");
    $stmt->execute(array(':short'=>$request->short));
    $result_url = $stmt->fetch();

    if (isset($result_url) && $result_url > 0) {
        return $response->code(423)->append('No more avalible');
    } else {
        $short = $request->short;

        $qry = $app->db->prepare('INSERT INTO urls (long, short, username) VALUES (?, ?, ?)');
        $qry->execute(array($request->long_url, $short, $user['username']));

        return $response->code(201)->append('https://url.itunix.eu/' . $short);
    }

});

$klein->respond('POST', '/generatekey', function ($request, $response, $service, $app) {
    // curl --data "token=admin_token" http://localhost/generatekey
    $ini = parse_ini_file('../app.ini');

    if ($request->token != $ini['admin_token']) {
        return $response->code(401)->append('Insufficient access to this query');
    }

    $apikey = bin2hex(openssl_random_pseudo_bytes(16));

    $qry = $app->db->prepare('INSERT INTO users (username, apikey, allowed) VALUES (?, ?, ?)');
    $qry->execute(array(time(), $apikey, 'allow'));

    return $response->code(201)->append($result. 'User API key created. API key: ' .  $apikey);
});

$klein->dispatch();

function getUrl($url, $preview, $response, $service, $app) {
    $stmt = $app->db->prepare("SELECT * FROM urls WHERE short = :short");
    $stmt->execute(array(':short'=>$url));
    $result = $stmt->fetch();

    if (isset($result) && $result > 0) {
        if ($preview) {
            $service->long_url = $result['long'];
            $service->render('../views/preview.phtml');
        } else {
            header("HTTP/1.1 301 Moved Permanently");
            header('Location:  '.  $result['long']  );
            die();
        }
    } else {
        return $response->code(404)->append('Nothing found');
    }
}

?>